//this file is used to add data to mongodb database from recipes-data.json

let mongoose = require('mongoose');
let fs = require('fs');
let path = require('path');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/Rest');

const recipes = JSON.parse(fs.readFileSync(path.join(__dirname + '/testdata.json'), 'utf-8'));

let Recipe = mongoose.model('Recipes', {
  id: {
    type: String
  },
  price: {
    type: String
  },
  picture: {
    type: String
  },
  name: {
    type: String
  },
  company: {
    type: String
  },
  email: {
    type: String
  },
  about: {
    type: String
  },
  registered: {
    type: String
  },
  size: {
    type: String
  }
});

async function loadRecipe() {
  try {
    await Recipe.insertMany(recipes);
    console.log('Done!');
    process.exit();
  } catch(e) {
    console.log(e);
    process.exit();
  }
}

loadRecipe();