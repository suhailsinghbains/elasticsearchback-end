let express = require('express');
let bodyParser = require('body-parser');
let mongoosastic = require("mongoosastic");
let elasticsearch = require('elasticsearch');

let app = express();
app.use(express.static('static_files'));
app.use(bodyParser.json());

let mongoose = require("mongoose");

mongoose.connect('mongodb://localhost:27017/Rest', { useNewUrlParser: true });

let Schema = mongoose.Schema;

let recipeSchema = new Schema({
  id: { type: String },
  price: { type: String },
  picture: { type: String },
  name: { type: String, es_indexed: true, es_type: 'text' },
  company: { type: String },
  email: { type: String },
  about: { type: String, es_indexed: true, es_type: 'text' },
  registered: { type: String },
  size: { type: String }
});

const esClient = new elasticsearch.Client({
  host: 'localhost:9200'
});
recipeSchema.plugin(mongoosastic, {
  esClient: esClient
});

let Recipe = mongoose.model("Recipes", recipeSchema),
  stream = Recipe.synchronize()
  , count = 0;

stream.on('data', function (err, doc) {
  count++;
});
stream.on('close', function () {
  console.log('indexed ' + count + ' documents!');
});
stream.on('error', function (err) {
  console.log(err);
});

app.get("/syncData", function (req, res) {
  let stream = Recipe.synchronize()
    , count = 0;

  stream.on('data', function (err, doc) {
    count++;
  });
  stream.on('close', function () {
    console.log('indexed ' + count + ' documents!');
  });
  stream.on('error', function (err) {
    console.log(err);
  });
  res.send({ result: "successfully indexed Mongo Data to ElasticSearch." });
});

let list = [];
let val, buckets;
app.get("/search/:term", function (req, res) {
  val = req.params.term;
  Query(val);
  res.send(list[0]);
  return list;
});
app.get("/suhail/:term", function(req, res){
  Query(val, req.params.term);
  res.send(list[0])
  return list;
});

function Query(val, agg="{}") {
  var Return;
  esClient.search({
    index: 'recipess',
    body: {
      query:
      {
        dis_max: {
          queries: [
            {
              match: {
                name: {
                  query: val,
                  boost: 5
                }
              }
            },
            {
              match: {
                ingredients: {
                  query: val,
                  boost: 1
                }
              }
            }, {
              multi_match: {
                query: val,
                fields: ["name", "ingredients"]
              }
            }
          ]
        }
      },
      aggs: {
        cookTime: {
          filters: {
            filters: [
              {
                match: {
                  body: {
                    query: agg
                  }
                }
              }
            ]
          }
        }
      }
    }
  }, function (error, response, status) {
    if (error) {
      console.log("search error: " + error)
    }
    else {
      console.log("--- Response ---");
      console.log(response);
      console.log("--- Hits ---");
      list.push(response.hits.hits);
      console.log(list)
      list[0].forEach(elt => {
        console.log(elt._source)
      });
      Return = list[0];
      buckets=[];
      buckets.push(response.aggregations.cookTime.buckets);
      // console.log(list[0]._source.name)
    }
  });
  return Return;
}

app.listen(3000, () => {
  console.log(`Started on port 3000`);
});