curl -X GET "localhost:9200/_search" -H 'Content-Type: application/json' -d'
{
      "query":
      {
        "dis_max": {
          "queries": [
            {
              "match": {
                "name": {
                  "query": "Best",
                  "boost": 5
                }
              }
            },
            {
              "match": {
                "ingredients": {
                  "query": "Best",
                  "boost": 1
                }
              }
            },{
              "multi_match":{
                "query": "Best",
                "fields": ["name", "ingredients"]
              }
            }
          ]
        }
      },
      "aggs": {
        "id":{
            "terms": {"field": "ingredients"}
        }
      }
    }
  
'
echo "\n"